call plug#begin()
  Plug 'Valloric/YouCompleteMe'
  Plug 'scrooloose/nerdtree' 
  Plug 'ctrlpvim/ctrlp.vim'
  Plug 'Xuyuanp/nerdtree-git-plugin'
  Plug 'airblade/vim-gitgutter'
	Plug 'majutsushi/tagbar'
	Plug 'tomasiser/vim-code-dark'
	Plug 'editorconfig/editorconfig-vim'
	Plug 'davidhalter/jedi-vim'
call plug#end()


let &colorcolumn=80
let mapleader = "."
hi ColorColumn guibg=#2d2d2d ctermbg=235
set cursorline 
set nu

" let g:jedi#use_splits_not_buffers = "bottom"

colorscheme codedark

" NERDTREE

autocmd vimenter * NERDTree
map <C-n> :NERDTreeToggle<CR>

let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'

nmap <F8> :TagbarToggle<CR>

set clipboard+=unnamed


" Ctr-P

set wildignore+=*/tmp/*,*.so,*.swp,*.zip     " MacOSX/Linux
set wildignore+=*\\tmp\\*,*.swp,*.zip,*.exe  " Windows
" For python develop 
set wildignore+=*.pyc,*.ipynb
let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/]\.(git|hg|svn)$',
  \ 'file': '\v\.(exe|so|dll)$',
  \ 'link': 'some_bad_symbolic_links',
  \ }



"
filetype plugin indent on


" use 4 spaces for tabs
set tabstop=4 softtabstop=4 shiftwidth=4

"avoid use esc key to exit insert mode
:imap jj <Esc>

" set python go to definitim
